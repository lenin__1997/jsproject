// Array
let city =['chennai','Madurai','Villupuram']
console.log(city.length)
console.log(city[2])
let matrix=[[1,2,3],[4,5,6],[7,8,9,0,0]]
console.log(matrix)
console.log(matrix[2][2]) //Always index start with value "0".
console.log(matrix[1][0]) //Always index start with value "0".
console.log(matrix.length)
console.log(matrix[2].length)

let array = ['a','b','c','d','e']
let array2 = ['a','b','c','d','e']
console.log(array.push('f')) //Add element at last and return the length
console.log(matrix[2].push('f'))  //Add element at this index return length of that index
console.log(array.pop()) //Remove Last element and return the element
console.log(array)
console.log(array.shift())   //Remove first element and return the element
console.log(array)
console.log(array.unshift('j')) //Add element at Start and return length
console.log(array) 
delete array[2]  // Delete and leave the place empty and defined
console.log(array[2])
//  Splice
//To delete value at index
console.log(array.splice(3,1)) //  in 3rd index delete one item return element
//Replace value at array
console.log(array.splice(1,1,'u')) // in 1st index remove 1 element then add "u" and return the removed element
//Add value in splice
console.log(array.splice(1,0,'b')) // in 1st index don't remove element then add "b"
let arr2=array.join();
// Split the content and convert it to Array
let arr3= "abcde";
console.log(arr3.split(''))
// Convert array to string also add "and" in the ","
console.log(array.join("and")) //Convert array to string .join

